Source: gdb
Maintainer: Héctor Orón Martínez <zumbi@debian.org>
Uploaders: Sergio Durigan Junior <sergiodj@debian.org>
Section: devel
Priority: optional
Standards-Version: 4.7.0
Rules-Requires-Root: binary-targets
Build-Depends:
# Packaging deps
               debhelper (>= 10),
               xz-utils,
# Other tool deps
               autoconf,
               libtool,
               gettext,
               bison,
               dejagnu,
               flex,
               procps,
               gobjc,
               mig [hurd-any],
# TeX[info] deps
               texinfo (>= 4.7-2.2),
               texlive-base <!nodoc>,
# Libdev deps
               libexpat1-dev,
               libncurses-dev,
               libreadline-dev,
               zlib1g-dev,
               liblzma-dev,
               libzstd-dev,
               libbabeltrace-dev,
               libipt-dev [amd64 i386 x32],
               libsource-highlight-dev,
	       libxxhash-dev,
	       libmpfr-dev,
               pkgconf,
# Python build
               python3-dev,
               libunwind-dev [ia64],
# debuginfod
               libdebuginfod-dev [linux-any],
# Reproducible build
               faketime,
Vcs-Git: https://salsa.debian.org/gdb-team/gdb.git
Vcs-Browser: https://salsa.debian.org/gdb-team/gdb
Homepage: https://www.gnu.org/s/gdb/

Package: gdb@TS@
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         ${python3:Depends}
Suggests: gdb-doc,
          gdbserver [linux-any],
	  libc-dbg
Breaks: gdb-minimal (<< 15.2-2~exp1)
Replaces: gdb-minimal (<< 15.2-2~exp1)
Provides: gdb-minimal
Description: GNU Debugger
 GDB is a source-level debugger, capable of breaking programs at
 any specific line, displaying variable values, and determining
 where errors occurred. Currently, gdb supports C, C++, D,
 Objective-C, Fortran, Java, OpenCL C, Pascal, assembly, Modula-2,
 Go, and Ada. A must-have for any serious programmer.

Package: gdb-multiarch
Architecture: any
Depends: gdb (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: GNU Debugger (with support for multiple architectures)
 GDB is a source-level debugger, capable of breaking programs at
 any specific line, displaying variable values, and determining
 where errors occurred. Currently, gdb supports C, C++, D,
 Objective-C, Fortran, Java, OpenCL C, Pascal, assembly, Modula-2,
 Go, and Ada. A must-have for any serious programmer.
 .
 This package contains a version of GDB which supports multiple
 target architectures.

Package: gdb-minimal
Architecture: all
Section: oldlibs
Multi-Arch: foreign
Depends: gdb (>= 15.2-2~exp1), ${misc:Depends}
Description: GNU Debugger (minimal version - dummy transitional package)
 GDB is a source-level debugger, capable of breaking programs at
 any specific line, displaying variable values, and determining
 where errors occurred. Currently, gdb supports C, C++, D,
 Objective-C, Fortran, Java, OpenCL C, Pascal, assembly, Modula-2,
 Go, and Ada. A must-have for any serious programmer.
 .
 This is a transitional package.  It can be safely removed.

Package: gdbserver
Architecture: amd64 armel armhf arm64 i386 ia64 m68k mips mipsel mips64el powerpc powerpcspe ppc64 ppc64el riscv64 s390 s390x x32
Depends: ${misc:Depends}, ${shlibs:Depends}
Replaces: gdb (<< 7.0.1-1)
Description: GNU Debugger (remote server)
 GDB is a source-level debugger, capable of breaking programs at
 any specific line, displaying variable values, and determining
 where errors occurred. Currently, gdb supports C, C++, D,
 Objective-C, Fortran, Java, OpenCL C, Pascal, assembly, Modula-2,
 Go, and Ada. A must-have for any serious programmer.
 .
 This package contains gdbserver.  Install this to debug remotely
 from another system where GDB is installed.

Package: gdb-source
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: GNU Debugger (source)
 GDB is a source-level debugger, capable of breaking programs at
 any specific line, displaying variable values, and determining
 where errors occurred. Currently, gdb supports C, C++, D,
 Objective-C, Fortran, Java, OpenCL C, Pascal, assembly, Modula-2,
 Go, and Ada. A must-have for any serious programmer.
 .
 This package contains the sources and patches which are needed
 to build GDB.
